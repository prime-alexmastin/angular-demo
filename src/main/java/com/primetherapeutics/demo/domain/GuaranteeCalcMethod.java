package com.primetherapeutics.demo.domain;

public enum GuaranteeCalcMethod {
    SCRIPTS,
    UNITS
}
