package com.primetherapeutics.demo.domain;

public enum GuaranteeBasis {
    REBATE_DOLLARS,
    REBATE_PLUS_ADMIN_DOLLARS
}
