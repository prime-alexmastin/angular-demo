package com.primetherapeutics.demo.domain;

public enum ClaimBasis {
    BRANDED,
    REBATE_EARNED,
    REBATE_ELIGIBLE,
    TOTAL_RECEIVED,
    TOTAL_RECEIVED_EXCEPT_EDITED
}
