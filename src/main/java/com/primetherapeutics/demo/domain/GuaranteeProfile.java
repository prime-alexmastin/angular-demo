package com.primetherapeutics.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GuaranteeProfile {
    private String name;
    private GuaranteeBasis guaranteeBasis;
    private ClaimBasis claimBasis;
    private GuaranteeCalcMethod guaranteeCalcMethod;
    private Boolean excludeClaims;
    private Date startDate;
    private Date endDate;
}
