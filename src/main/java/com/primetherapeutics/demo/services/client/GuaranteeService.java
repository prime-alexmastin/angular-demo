package com.primetherapeutics.demo.services.client;

import com.primetherapeutics.demo.domain.Guarantee;

import java.util.List;

public interface GuaranteeService {
    List<Guarantee> findAll();
}
