import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PoGuaranteeComponent } from './po-guarantee/po-guarantee.component';
import { PoGuaranteeProfileComponent } from './po-guarantee-profile/po-guarantee-profile.component';
import { MatExpansionModule } from "@angular/material/expansion";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {GuaranteeService} from "./service/guarantee.service";
import {MatInputModule} from "@angular/material/input";
import {MatRadioModule} from "@angular/material/radio";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatNativeDateModule} from "@angular/material/core";


@NgModule({
  declarations: [
    AppComponent,
    PoGuaranteeComponent,
    PoGuaranteeProfileComponent
  ],
    imports: [
        FormsModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatExpansionModule,
        MatButtonModule,
        MatFormFieldModule,
        MatTableModule,
        MatPaginatorModule,
        MatInputModule,
        MatRadioModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatGridListModule,
        MatNativeDateModule,
        ReactiveFormsModule
    ],
  providers: [GuaranteeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
