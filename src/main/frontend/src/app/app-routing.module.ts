import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PoGuaranteeComponent} from "./po-guarantee/po-guarantee.component";


const routes: Routes = [
  { path: 'guarantee', component: PoGuaranteeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
