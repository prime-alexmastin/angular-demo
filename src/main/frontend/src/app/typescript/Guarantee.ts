import {GuaranteeProfile} from "./GuaranteeProfile";

export class Guarantee {
    name: String;
    id: String;
    startDate: Date;
    endDate: Date;
    guaranteeProfile: GuaranteeProfile;
}
