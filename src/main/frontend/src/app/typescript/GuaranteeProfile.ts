import {GuaranteeBasis} from "./GuaranteeBasis";
import {ClaimBasis} from "./ClaimBasis";
import {GuaranteeCalcMethod} from "./GuaranteeCalcMethod";

export class GuaranteeProfile {
    name: String;
    guaranteeBasis: GuaranteeBasis;
    claimBasis: ClaimBasis;
    guaranteeCalcMethod: GuaranteeCalcMethod;
    excludeClaims: Boolean;
    startDate: Date;
    endDate: Date;
}
