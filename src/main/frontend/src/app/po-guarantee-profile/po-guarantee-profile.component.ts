import { Component, OnInit } from '@angular/core';
import {GuaranteeProfile} from "../typescript/GuaranteeProfile";
import {MatTableDataSource} from "@angular/material/table";
import {GuaranteeService} from "../service/guarantee.service";
import {FormControl, Validators} from "@angular/forms";


@Component({
  selector: 'po-guarantee-profile',
  templateUrl: './po-guarantee-profile.component.html',
  styleUrls: ['./po-guarantee-profile.component.scss']
})
export class PoGuaranteeProfileComponent implements OnInit {
  items: GuaranteeProfile[] = [];
  headers: string[] = ['name', 'id', 'startDate', 'endDate'];
  dataSource;
  selectedItem: GuaranteeProfile = {
    name: '',
    guaranteeBasis: null,
    claimBasis: null,
    guaranteeCalcMethod: null,
    excludeClaims: false,
    startDate: null,
    endDate: null,
  };

  constructor(private service: GuaranteeService) { }

  ngOnInit() {
    this.service.selectedGuarantee.subscribe(guarantee => {
      if (guarantee.guaranteeProfile) {
        this.items = [];
        this.items.push(guarantee.guaranteeProfile);
        console.log(this.items);

        this.dataSource = new MatTableDataSource<GuaranteeProfile>(this.items);
      }
    });
  }

  selectItem(item) {
    Object.assign(this.selectedItem, item);
  }

  clear() {
    this.selectedItem = {
      name: '',
      guaranteeBasis: null,
      claimBasis: null,
      guaranteeCalcMethod: null,
      excludeClaims: false,
      startDate: null,
      endDate: null,
    };
  }

  save() {
    // Example save:
    //
    // let self = this;
    // axios.post('http://localhost:8080/api/vendor/guarantee')
    //     .then((response) => {
    //         self.items = response.data;
    //     })
    //     .catch((error) => {
    //         console.log(error);
    //     })
  }
}
