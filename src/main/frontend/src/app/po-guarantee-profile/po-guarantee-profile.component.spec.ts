import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoGuaranteeProfileComponent } from './po-guarantee-profile.component';

describe('PoGuaranteeProfileComponent', () => {
  let component: PoGuaranteeProfileComponent;
  let fixture: ComponentFixture<PoGuaranteeProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoGuaranteeProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoGuaranteeProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
