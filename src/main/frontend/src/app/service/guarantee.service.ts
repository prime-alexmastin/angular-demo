import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {Guarantee} from '../typescript/Guarantee'

@Injectable()
export class GuaranteeService {
    private guarantee = new BehaviorSubject(new Guarantee());
    selectedGuarantee = this.guarantee.asObservable();

    constructor() { }

    selectGuarantee(guarantee: Guarantee) {
        this.guarantee.next(guarantee);
    }
}
