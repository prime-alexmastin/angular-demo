import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Guarantee} from '../typescript/Guarantee'
import {GuaranteeService} from "../service/guarantee.service";
import axios from 'axios'

@Component({
  selector: 'po-guarantee',
  templateUrl: './po-guarantee.component.html',
  styleUrls: ['./po-guarantee.component.scss']
})
export class PoGuaranteeComponent implements OnInit {
  items: Guarantee[] = [];
  headers: string[] = ['name', 'id', 'startDate', 'endDate'];
  dataSource;

  constructor(private service: GuaranteeService) { }

  ngOnInit() {
    axios.get("http://localhost:8080/api/vendor/guarantee")
        .then((response) => {
          response.data.forEach((guarantee: Guarantee) => {
           this.items.push(guarantee);
          })
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
            this.dataSource = new MatTableDataSource<Guarantee>(this.items);
        })
  }

  selectItem(guarantee: Guarantee) {
    this.service.selectGuarantee(guarantee);
  }

}
