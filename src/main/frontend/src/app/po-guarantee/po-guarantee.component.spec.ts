import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoGuaranteeComponent } from './po-guarantee.component';

describe('PoGuaranteeComponent', () => {
  let component: PoGuaranteeComponent;
  let fixture: ComponentFixture<PoGuaranteeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoGuaranteeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoGuaranteeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
